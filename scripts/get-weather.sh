#!/bin/bash

curl -X GET https://api.openweathermap.org/data/2.5/weather?appid=$WEATHER_API\&q=chicago \
-H "Accept: application/json" \
| ruby -e "require 'json'; hashed = JSON.parse(ARGF.read); puts hashed['weather'][0]['description']"

