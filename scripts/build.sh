#!/bin/bash

mkdir ./build
cp *.html ./build
touch build/VERSION && echo $CI_COMMIT_SHA >> build/VERSION
