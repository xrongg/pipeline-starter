#!/bin/bash

if [[ $(date +%u) -gt 5 ]]; then
    echo 'Sorry, you cannot do this on the weekend.'
    exit 1
else
	echo 'Weekday work? Go get em, tiger!'
fi